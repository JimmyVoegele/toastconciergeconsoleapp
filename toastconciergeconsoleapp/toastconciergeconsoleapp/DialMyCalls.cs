﻿using System;
using System.Data;
using System.Collections.Generic;

using IO.DialMyCalls.Api;
using IO.DialMyCalls.Client;
using IO.DialMyCalls.Model;
using System.Diagnostics;


namespace toastconciergeconsoleapp
{
    public class ContactAttributesEX
    {
        private ContactAttributes m_CA;
        private int m_SchoolID;
        private bool m_PilotStudent;

        public ContactAttributesEX(string Phone = null, string Firstname = null, string Lastname = null, string Email = null, int SchoolID = 0, bool PilotStudent = false)
        {
            m_CA = new ContactAttributes(Phone, Firstname, Lastname, Email);
            m_SchoolID = SchoolID;
            m_PilotStudent = PilotStudent;
        }

        public ContactAttributes CA
        {
            get
            {
                return m_CA;
            }
        }

        public int SchoolID
        {
            get
            {
                return m_SchoolID;
            }
        }

        public bool PilotStudent
        {
            get
            {
                return m_PilotStudent;
            }
        }
    }

    public class DialMyCalls
    {
        //private string m_strCallerID = string.Empty;
        private string m_strApiKey = string.Empty;


        public DialMyCalls()
        {

        }

        public void LoadAPIKey(string strAPIKey)
        {
            bool bError = false;

            if (!string.IsNullOrEmpty(strAPIKey))
            {

                try
                {
                    m_strApiKey = strAPIKey;
                    Configuration.Default.ApiKey.Add("X-Auth-ApiKey", strAPIKey);
                }
                catch (Exception ex)
                {
                    Debug.Print("Already Loaded" + ex.Message);
                    bError = true;
                }

                if (bError)
                {
                    Configuration.Default.ApiKey["X-Auth-ApiKey"] = strAPIKey;
                }
            }


        }

        public string GetAPIKey()
        {
            return m_strApiKey;
        }

        private string GetID(string strContainer)
        {
            string[] stringSeparators = new string[] { "\r\n" };

            var ContainerString = strContainer.Split(stringSeparators, StringSplitOptions.None);

            string strGuid = string.Empty;

            foreach (string strElement in ContainerString)
            {
                if (strElement.ToLower().Contains("\"id"))
                {
                    strGuid = strElement.Substring(10);
                }

            }
            return CleanElement(strGuid);
        }

        public DataTable GetContacts()
        {
            var apiInstance = new Contacts();

            DataTable DT = new DataTable();

            DT.Columns.Add("FirstName");
            DT.Columns.Add("Lastname");
            DT.Columns.Add("Phone");
            DT.Columns.Add("Email");
            DT.Columns.Add("GUID");
            bool bContactAdded = true;

            for (int i = 0; i < 100; i++)
            {
                if (bContactAdded)
                {
                    bContactAdded = false;
                }
                else
                    break;
                int min = i * 250;
                int max = i * 250 + 250;
                string strRange = "records=" + min.ToString() + "-" + max.ToString();

                bContactAdded = false;

                var Contacts = apiInstance.GetContacts(strRange);

                var str = Contacts.ToString();

                string[] stringSeparators = new string[] { "\r\n" };

                var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

                string strFirstName = string.Empty;
                string strLastName = string.Empty;
                string strPhoneNumber = string.Empty;
                string strEmail = string.Empty;
                string strGuid = string.Empty;



                foreach (string strElement in ContactString)
                {
                    if (strElement.ToLower().Contains("\"id\""))
                    {
                        strGuid = strElement.Substring(10);
                    }

                    if (strElement.ToLower().Contains("\"firstname"))
                    {
                        strFirstName = strElement.Substring(17);
                    }
                    if (strElement.ToLower().Contains("\"lastname"))
                    {
                        strLastName = strElement.Substring(17);
                    }

                    if (strElement.ToLower().Contains("\"phone"))
                    {
                        strPhoneNumber = strElement.Substring(15);
                    }

                    if (strElement.ToLower().Contains("\"email"))
                    {
                        strEmail = strElement.Substring(15);
                        DT.Rows.Add(CleanElement(strFirstName), CleanElement(strLastName), CleanElement(strPhoneNumber), CleanElement(strEmail), CleanElement(strGuid));
                        bContactAdded = true;
                    }

                }
            }
            return DT;
        }

        public string GetContactID(string strPhoneNumber)
        {
            var apiInstance = new Contacts();
            //var Contacts = apiInstance.GetContacts("records=201-300");


            for (int i = 0; i < 100; i++)
            {
                int min = i * 250;
                int max = i * 250 + 250;
                string strRange = "records=" + min.ToString() + "-" + max.ToString();
                var ContactsNew = apiInstance.GetContacts(strRange);


                var str = ContactsNew.ToString();

                string[] stringSeparators = new string[] { "\r\n" };

                var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

                string strFirstName = string.Empty;
                string strLastName = string.Empty;
                string strEmail = string.Empty;
                string strGuid = string.Empty;

                foreach (string strElement in ContactString)
                {
                    Debug.Write(strElement);
                    if (strElement.ToLower().Contains("\"id"))
                    {
                        strGuid = strElement.Substring(10);
                    }

                    if (strElement.ToLower().Contains("\"phone") && strElement.Contains(strPhoneNumber))
                    {
                        strPhoneNumber = strElement.Substring(15);
                        return CleanElement(strGuid);
                    }
                }
            }

            return string.Empty;
        }

        private string CleanElement(string strContact)
        {
            return CleanElement(strContact, false);
        }

        private string CleanElement(string strContact, bool bDateTimeElement)
        {
            string strCleanContact = string.Empty;

            if (!bDateTimeElement)
                strCleanContact = strContact.Replace("\"firstname\"", "").Replace("\"lastname\"", "").Replace("\"phone\"", "").Replace("\"email\"", "").Replace(".", "").Replace(":", "").Replace("\"", "").Replace(",", "");
            else
                strCleanContact = strContact.Replace("\"firstname\"", "").Replace("\"lastname\"", "").Replace("\"phone\"", "").Replace("\"email\"", "").Replace(".", "").Replace("\"", "").Replace(",", "");

            return strCleanContact.Trim();
        }

        //public void SetCallerID(string strCallerID)
        //{
        //    m_strCallerID = strCallerID;
        //}

        public Guid GetCallerID()
        {
            var apiInstanceCallerIDs = new CallerIds();

            var CallerIDs = apiInstanceCallerIDs.GetCallerIds();

            string ID = CallerIDs.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var CallerIDString = ID.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in CallerIDString)
            {
                if (strElement.ToLower().Contains("\"id"))
                {
                    strID = CleanElement(strElement.Substring(10));
                }
            }

            Guid g = new Guid(strID);

            return g;
        }

        public string CreateGroup(string strGroupName)
        {
            var apiInstance = new Groups();

            string strGrooupID = GetGroupID(strGroupName);

            if (string.IsNullOrEmpty(strGrooupID))
            {
                var createGroupParameters = new CreateGroupParameters(strGroupName);

                apiInstance.CreateGroup(createGroupParameters);

                strGrooupID = GetGroupID(strGroupName);
            }
            return strGrooupID;
        }

        public string GetRecordingName(string strRecordingGUID)
        {
            string strRecordingName = string.Empty;

            var apiInstanceRecordings = new Recordings();

            try
            {
                // Get Recording
                Object result = apiInstanceRecordings.GetRecordingById(strRecordingGUID);
                var str = result.ToString();

                string[] stringSeparators = new string[] { "\r\n" };

                var RecordingString = str.Split(stringSeparators, StringSplitOptions.None);

                foreach (string strElement in RecordingString)
                {
                    if (strElement.ToLower().Contains("\"name"))
                    {
                        strRecordingName = strElement.Substring(13);
                        return CleanElement(strRecordingName);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Recordings.GetRecordingById: " + e.Message);
            }

            return strRecordingName;
        }

        public Dictionary<string, string> GetRecordings()
        {
            Dictionary<string, string> dct = new Dictionary<string, string>();

            var apiInstanceRecordings = new Recordings();
            var Recordings = apiInstanceRecordings.GetRecordings();

            var str = Recordings.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("name"))
                {
                    strName = strElement.Substring(13); ;
                    dct.Add(CleanElement(strID.Trim()), CleanElement(strName.Trim()));
                }
            }

            return dct;
        }

        public Dictionary<string, string> GetGroups()
        {
            Dictionary<string, string> dct = new Dictionary<string, string>();

            var apiInstance = new Groups();
            var Groups = apiInstance.GetGroups();

            var str = Groups.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("name"))
                {
                    strName = strElement.Substring(13); ;
                    dct.Add(CleanElement(strID.Trim()), CleanElement(strName.Trim()));
                }
            }

            return dct;
        }

        public string GetGroupID(string strGroupName)
        {
            Dictionary<string, string> dct = new Dictionary<string, string>();

            var apiInstance = new Groups();
            var Groups = apiInstance.GetGroups();

            var str = Groups.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("name") && (strElement.ToLower().Contains(strGroupName.ToLower())))
                {
                    strName = strElement.Substring(13); ;
                    return CleanElement(strID);
                }
            }

            return string.Empty;
        }
        public bool CreateContact(string strFirstName, string strLastName, string strPhoneNumber, string strExtension, string strEmail)
        {
            return CreateContact(strFirstName, strLastName, strPhoneNumber, strExtension, strEmail, null);
        }

        public bool CreateContact(string strFirstName, string strLastName, string strPhoneNumber, string strExtension, string strEmail, List<string> strGroup)
        {
            var apiInstance = new Contacts();
            string strPhoneNumberClean = strPhoneNumber.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
            var createContactParameters = new CreateContactParameters(strFirstName, strLastName, strPhoneNumberClean, strExtension, strEmail, null, strGroup); // CreateContactParameters | Request body

            try
            {
                // Add Contact
                Object result = apiInstance.CreateContact(createContactParameters);
                Debug.WriteLine(result);
            }
            catch (Exception ex)
            {
                Debug.Print("Exception when calling Contacts.CreateContact: " + ex.Message);
                if (ex.Message.Contains("This phone number already exists as a contact."))
                {
                    try
                    {
                        string strGUID = GetContactID(strPhoneNumber);
                        var upDateContactParameters = new UpdateContactByIdParameters(strFirstName, strLastName, strPhoneNumber, strExtension, strEmail, null, strGroup);
                        apiInstance.UpdateContactById(upDateContactParameters, strGUID);
                    }
                    catch (Exception EX)
                    {
                        Debug.Print("Exception when calling Contacts.CreateContact: " + EX.Message);
                        return false;
                    }
                }
                else
                {
                    Debug.Print("Exception when calling Contacts.CreateContact: " + ex.Message);
                    return false;
                }

            }

            return true;
        }

        public string DeleteAllContacts()
        {
            var apiInstance = new Contacts();

            try
            {
                DataTable DT = GetContacts();
                // Add Contact
                foreach (DataRow DR in DT.Rows)
                {
                    try
                    {
                        Object result = apiInstance.DeleteContactById(DR["GUID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.Write("123" + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Contacts deleted successfully.";

        }
        public string DeleteAllGroups()
        {
            var apiInstance = new Groups();

            try
            {
                Dictionary<string, string> DT = GetGroups();
                // Add Contact
                foreach (KeyValuePair<string, string> kvp in DT)
                {
                    try
                    {
                        Object result = apiInstance.DeleteGroupById(kvp.Key);
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Group deleted successfully.";

        }

        public string DeleteContact(string strContactID)
        {
            var apiInstance = new Contacts();

            try
            {
                // Add Contact
                Object result = apiInstance.DeleteContactById(strContactID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Contact deleted successfully.";

        }


        public void SendText(string strBroadcastName, Guid gKeywordID, string strMessage, List<ContactAttributesEX> lstCA)
        {
            SendText(strBroadcastName, gKeywordID, strMessage, lstCA, null);
        }

        public Dictionary<string, string> GetTextKeyWords()
        {
            var apiKeyword = new Keywords();

            var ID = apiKeyword.GetKeywords("");

            Dictionary<string, string> dct = new Dictionary<string, string>();

            var str = ID.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var KeywordString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in KeywordString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("keyword"))
                {
                    strName = strElement.Substring(15);
                    dct.Add(CleanElement(strID.Trim()), CleanElement(strName.Trim()));
                }
            }

            return dct;
        }

        public string SendText(string strBroadcastName, Guid gKeywordID, string strMessage, List<ContactAttributesEX> lstCAEX, string strDateTime)
        {
            var apiInstance = new Texts();
            //var apiKeyword = new Keywords();

            //var ID = apiKeyword.GetKeywords("DMC9H6F6");
            List<ContactAttributes> lstCA = new List<ContactAttributes>();

            foreach (ContactAttributesEX CA in lstCAEX)
            {
                lstCA.Add(CA.CA);
            }


            List<string> lst = new List<string>();
            if (strMessage.Length < 150)
            {
                lst.Add(strMessage.Trim().Substring(0, strMessage.Length));
            }
            else
            {
                int iMessages = strMessage.Length / 150;
                for (int i = 0; i <= iMessages; i++)
                {
                    if (i == iMessages)
                    {
                        lst.Add(strMessage.Trim().Substring(i * 150));
                    }
                    else
                    {
                        lst.Add(strMessage.Trim().Substring(i * 150, 150));
                    }
                }
            }


            CreateTextParameters createTextParameters = null;
            if (string.IsNullOrEmpty(strDateTime))
            {
                createTextParameters = new CreateTextParameters(strBroadcastName, gKeywordID, lst, null, true, false, null, null, null, lstCA); // CreateTextParameters | Request body
            }
            else
            {
                DateTime dt = Convert.ToDateTime(strDateTime);
                string dtUTC = dt.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss+0000");
                createTextParameters = new CreateTextParameters(strBroadcastName, gKeywordID, lst, dtUTC, false, false, null, null, null, lstCA); // CreateTextParameters | Request body
            }

            try
            {
                // Create Text
                Object result = apiInstance.CreateText(createTextParameters);
                string strGUID = GetID(result.ToString());
                return strGUID;
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Texts.CreateText: " + e.Message);
                return e.Message;
            }
        }

        public void MakeCall(string strBroadcastName, Guid CallerID, Guid RecordingID, List<ContactAttributesEX> lstCA)
        {
            MakeCall(strBroadcastName, CallerID, RecordingID, lstCA, null);
        }

        public string MakeCall(string strBroadcastName, Guid CallerID, Guid RecordingID, List<ContactAttributesEX> lstCAEX, string strDateTime)
        {
            var apiInstance = new Calls();
            var apiInstanceCallerID = new CallerIds();


            var apiInstanceList = apiInstanceCallerID.GetCallerIds();

            List<ContactAttributes> lstCA = new List<ContactAttributes>();

            foreach (ContactAttributesEX CA in lstCAEX)
            {
                lstCA.Add(CA.CA);
            }

            CreateCallParameters createCallParameters = null;
            if (string.IsNullOrEmpty(strDateTime))
            {
                createCallParameters = new CreateCallParameters(strBroadcastName, CallerID, RecordingID, null, null, true, true, true, null, lstCA); // CreateCallParameters | Request body
            }
            else
            {
                DateTime dt = Convert.ToDateTime(strDateTime);
                string dtUTC = dt.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss+0000");
                createCallParameters = new CreateCallParameters(strBroadcastName, CallerID, RecordingID, null, dtUTC, false, true, true, null, lstCA); // CreateCallParameters | Request body
            }

            try
            {
                // Create Call
                Object result = apiInstance.CreateCall(createCallParameters);
                Debug.WriteLine(result);
                string strGUID = GetID(result.ToString());
                return strGUID;
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Calls.CreateCall: " + e.Message);
                return e.Message;
            }

        }

        public DataTable GetCallStatus(string strCallID)
        {

            var apiInstance = new Calls();

            //var NewCalls = apiInstance.GetCalls();
            //62ab19c0-8dc4-11e7-97ce-0cc47a8124e8
            var Calls = apiInstance.GetCallRecipientsByCallId(strCallID);

            return ParseHistory(Calls.ToString(), false);
        }

        public DataTable GetTextStatus(string strTextID)
        {
            var apiInstanceText = new Texts();

            var Texts = apiInstanceText.GetTextRecipientsByTextId(strTextID);

            return ParseHistory(Texts.ToString(), true);
        }

        private DataTable ParseHistory(string strValues, bool bTextHistory)
        {
            string[] stringSeparators = new string[] { "\r\n" };

            var CallString = strValues.Split(stringSeparators, StringSplitOptions.None);

            string strFirstName = string.Empty;
            string strLastName = string.Empty;
            string strPhoneNumber = string.Empty;
            string strGrade = string.Empty;

            string strStatus = string.Empty;
            string strDuration = string.Empty;
            string strAttempts = string.Empty;
            string strSuccessful = string.Empty;
            string strCalledAt = string.Empty;

            DataTable DT = new DataTable();

            DT.Columns.Add("FirstName");
            DT.Columns.Add("Lastname");
            DT.Columns.Add("Phone");
            DT.Columns.Add("Grade");

            DT.Columns.Add("Status");
            DT.Columns.Add("Duration");
            DT.Columns.Add("Attempts");
            DT.Columns.Add("successful");
            DT.Columns.Add("CalledAt");

            foreach (string strElement in CallString)
            {
                if (strElement.ToLower().Contains("\"firstname"))
                {
                    strFirstName = strElement.Substring(17);
                }
                if (strElement.ToLower().Contains("\"lastname"))
                {
                    strLastName = strElement.Substring(17);
                }

                if (strElement.ToLower().Contains("\"phone"))
                {
                    strPhoneNumber = strElement.Substring(15);
                }

                if (strElement.ToLower().Contains("\"email"))
                {
                    strGrade = strElement.Substring(15);
                }

                if (strElement.ToLower().Contains("\"status"))
                {
                    strStatus = strElement.Substring(15);
                }
                if (strElement.ToLower().Contains("\"duration"))
                {
                    strDuration = strElement.Substring(17);
                }

                if (strElement.ToLower().Contains("\"attempts"))
                {
                    strAttempts = strElement.Substring(15);
                }

                if (strElement.ToLower().Contains("\"successful"))
                {
                    strSuccessful = CleanElement(strElement.Substring(17));
                    if (strSuccessful.ToLower() != "true")
                    {
                        string strStatusOveride = CleanElement(strStatus);

                        if (strStatusOveride.ToLower() == "undeliverable" || strStatusOveride.ToLower() == "non_mobile" || strStatusOveride.ToLower() == "no_answer" || strStatusOveride.ToLower() == "busy")
                        {
                            strSuccessful = "true";
                        }
                        else
                        {
                            strSuccessful = "false";
                        }
                    }
                    if (bTextHistory)
                    {
                        strCalledAt = string.Empty;
                        DT.Rows.Add(CleanElement(strFirstName), CleanElement(strLastName), CleanElement(strPhoneNumber), CleanElement(strGrade), CleanElement(strStatus), CleanElement(strDuration), CleanElement(strAttempts), CleanElement(strSuccessful), CleanElement(strCalledAt, true));
                    }
                }

                if (strElement.ToLower().Contains("\"called_at"))
                {
                    strCalledAt = strElement.Substring(18);
                    DT.Rows.Add(CleanElement(strFirstName), CleanElement(strLastName), CleanElement(strPhoneNumber), CleanElement(strGrade), CleanElement(strStatus), CleanElement(strDuration), CleanElement(strAttempts), CleanElement(strSuccessful), CleanElement(strCalledAt, true));
                }
            }

            return DT;
        }
    }
}