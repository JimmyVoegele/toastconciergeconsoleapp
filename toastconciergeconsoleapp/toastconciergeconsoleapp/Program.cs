﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Mail;


namespace toastconciergeconsoleapp
{
    static class Program
    {
       
        static void Main(string[] args)
        {
            
            //SendErrorStatus("Jimmy", "Subject", true);
            //SendText("Welcome to Toast of the Town! To learn more visit https://toastaustin.org", "4052503881");
            if (args.Length > 0)
            {
                foreach (string str in args)
                {
                    if (str.ToUpper().Contains("COMMCENTER"))
                    {
                        ProcessMessageQueue();
                    }

                    if (str.ToUpper().Contains("REQUESTREMINDER"))
                    {
                        //ProcessRequestReminderQueue();
                    }

                }
            }




        }

        static void ProcessRequestReminderQueue()
        {
            ProcessRequestReminderEmailQueue();
            ProcessRequestReminderTextQueue();
        }

        static void ProcessRequestReminderEmailQueue()
        {
            string selectSQL = string.Empty;

            selectSQL = "Select distinct Email from vwPartySlots";
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                string EmailAddress = "";
                Hashtable htRecipients = new Hashtable();
                while (reader.Read())
                {
                    EmailAddress = reader["Email"].ToString().Trim();
                    htRecipients.Add(EmailAddress, EmailAddress);
                }
                htRecipients = new Hashtable();


                ////htRecipients.Add("ibergcathy@gmail.com", "ibergcathy@gmail.com");
                //htRecipients.Add("psutton@austincc.edu", "psutton@austincc.edu");
                //htRecipients.Add("bobbiejbarker@gmail.com", "bobbiejbarker@gmail.com");
                //htRecipients.Add("kathlenecrist@yahoo.com", "kathlenecrist@yahoo.com");
                //htRecipients.Add("dwfreid@aol.com", "dwfreid@aol.com");
                //htRecipients.Add("andygreenawalt@earthlink.net", "andygreenawalt@earthlink.net");
                //htRecipients.Add("maryann@heller.com", "maryann@heller.com");
                //htRecipients.Add("dhuffs@aol.com", "dhuffs@aol.com");
                //htRecipients.Add("ibergcathy@gmail.com", "ibergcathy@gmail.com");
                //htRecipients.Add("johnrmarietta@gmail.com", "johnrmarietta@gmail.com");
                //htRecipients.Add("dick@enovateenterprises.com", "dick@enovateenterprises.com");
                //htRecipients.Add("caroline.murphy@stdavids.com", "caroline.murphy@stdavids.com");
                //htRecipients.Add("randyr@txassoc.com", "randyr@txassoc.com");
                //htRecipients.Add("atskel5@gmail.com", "atskel5@gmail.com");
                //htRecipients.Add("torrie.rebuck@superiorhealthplan.com", "torrie.rebuck@superiorhealthplan.com");
                //htRecipients.Add("kathy-taylor@austin.rr.com", "kathy-taylor@austin.rr.com");
                //htRecipients.Add("jandonwilk@aol.com", "jandonwilk@aol.com");
                //htRecipients.Add("cjw7000@gmail.com", "cjw7000@gmail.com");
                //htRecipients.Add("s202cd@heb.com", "s202cd@heb.com");

                htRecipients.Add("a.soares@clinpathassoc.com", "a.soares@clinpathassoc.com");
                htRecipients.Add("ACooper@tyrexmfg.com", "ACooper@tyrexmfg.com");
                htRecipients.Add("AmyT@calendars.com", "AmyT@calendars.com");
                htRecipients.Add("aramintasellers@icloud.com", "aramintasellers@icloud.com");
                htRecipients.Add("atskel5@gmail.com", "atskel5@gmail.com");
                htRecipients.Add("avanivije@gmail.com", "avanivije@gmail.com");
                htRecipients.Add("barbaraportersfs@gmail.com", "barbaraportersfs@gmail.com");
                htRecipients.Add("beeprice@grandecom.net ", "beeprice@grandecom.net ");
                htRecipients.Add("bgabell@aol.com", "bgabell@aol.com");
                htRecipients.Add("biz@darrickmcgill.com", "biz@darrickmcgill.com");
                htRecipients.Add("bobbiejbarker@gmail.com", "bobbiejbarker@gmail.com");
                htRecipients.Add("caroline.murphy@stdavids.com", "caroline.murphy@stdavids.com");
                htRecipients.Add("carrieluchristensen@hotmail.com", "carrieluchristensen@hotmail.com");
                htRecipients.Add("casj3@swbell.net", "casj3@swbell.net");
                htRecipients.Add("cb@austin.rr.com", "cb@austin.rr.com");
                htRecipients.Add("chandra.hosek@gmail.com", "chandra.hosek@gmail.com");
                htRecipients.Add("chuck.lipscomb@jedunn.com", "chuck.lipscomb@jedunn.com");
                htRecipients.Add("cynthia@taxmantom.com", "cynthia@taxmantom.com");
                htRecipients.Add("denise.bradley@stdavids.com", "denise.bradley@stdavids.com");
                htRecipients.Add("denisedenise.bradley@stdavids.com ", "denisedenise.bradley@stdavids.com ");
                htRecipients.Add("dhuffs@aol.com", "dhuffs@aol.com");
                htRecipients.Add("dick@enovateenterprises.com", "dick@enovateenterprises.com");
                htRecipients.Add("dwfreid@aol.com", "dwfreid@aol.com");
                htRecipients.Add("gasman1937@yahoo.com", "gasman1937@yahoo.com");
                htRecipients.Add("jandonwilk@aol.com", "jandonwilk@aol.com");
                htRecipients.Add("jennenem@att.net", "jennenem@att.net");
                htRecipients.Add("jlangseth@auroradx.com", "jlangseth@auroradx.com");
                htRecipients.Add("jnsumurray@sbcglobal.net", "jnsumurray@sbcglobal.net");
                htRecipients.Add("Johnrmarietta@gmail.com", "Johnrmarietta@gmail.com");
                htRecipients.Add("kane@aqcaustin.com", "kane@aqcaustin.com");
                htRecipients.Add("kaylen@txfertility.com", "kaylen@txfertility.com");
                htRecipients.Add("kozmun@hahnpublic.com", "kozmun@hahnpublic.com");
                htRecipients.Add("krforgione@gmail.com", "krforgione@gmail.com");
                htRecipients.Add("ktaylor@pinnergy.com", "ktaylor@pinnergy.com");
                htRecipients.Add("kvaughn0511@aol.com", "kvaughn0511@aol.com");
                htRecipients.Add("lbutler@frostbank.com", "lbutler@frostbank.com");
                htRecipients.Add("lesliek@aaobgyn.com", "lesliek@aaobgyn.com");
                htRecipients.Add("lewlittlejr@gmail.com", "lewlittlejr@gmail.com");
                htRecipients.Add("Linda@gthf.org", "Linda@gthf.org");
                htRecipients.Add("lisasavagemd@gmail.com", "lisasavagemd@gmail.com");
                htRecipients.Add("megcw@live.com", "megcw@live.com");
                htRecipients.Add("nickimebane@grandecom.net", "nickimebane@grandecom.net");
                htRecipients.Add("paultcarrell@gmail.com", "paultcarrell@gmail.com");
                htRecipients.Add("ppincoffs@austin.rr.com", "ppincoffs@austin.rr.com");
                htRecipients.Add("prfreel@yahoo.com", "prfreel@yahoo.com");
                htRecipients.Add("psutton@austincc.edu", "psutton@austincc.edu");
                htRecipients.Add("ray.bonilla@sbcglobal.net", "ray.bonilla@sbcglobal.net");
                htRecipients.Add("renata@myvillaaustin.com", "renata@myvillaaustin.com");
                htRecipients.Add("rey_garza@mednax.com", "rey_garza@mednax.com");
                htRecipients.Add("sachen9@gmail.com", "sachen9@gmail.com");
                htRecipients.Add("sarar@austin.rr.com", "sarar@austin.rr.com");
                htRecipients.Add("scott@gthf.org", "scott@gthf.org");
                htRecipients.Add("scottbale@me.com", "scottbale@me.com");
                htRecipients.Add("sgupta@stdavidsfoundation.org", "sgupta@stdavidsfoundation.org");
                htRecipients.Add("sharon@austinprotocol.com", "sharon@austinprotocol.com");
                htRecipients.Add("shermbond@gmail.com", "shermbond@gmail.com");
                htRecipients.Add("sknebel@mlrpc.com", "sknebel@mlrpc.com");
                htRecipients.Add("teresa.holmes@centexobgyn.com", "teresa.holmes@centexobgyn.com");
                htRecipients.Add("thunt@aol.com", "thunt@aol.com");
                htRecipients.Add("tmfunte73@yahoo.com", "tmfunte73@yahoo.com");
                htRecipients.Add("torrie.rebuck@superiorhealthplan.com", "torrie.rebuck@superiorhealthplan.com");
                htRecipients.Add("wilkinsons@ausrad.com", "wilkinsons@ausrad.com");
                htRecipients.Add("wilsonanng@gmail.com", "wilsonanng@gmail.com");
                htRecipients.Add("zachary.sussman@gmail.com", "zachary.sussman@gmail.com");


                //htRecipients.Add("twaldron@stdavidsfoundation.org", "twaldron@stdavidsfoundation.org");
                //htRecipients.Add("tgutierrez@stdavidsfoundation.org", "tgutierrez@stdavidsfoundation.org");
                //htRecipients.Add("AGomez@stdavidsfoundation.org", "AGomez@stdavidsfoundation.org");
                //htRecipients.Add("cabazari@stdavidsfoundation.org", "cabazari@stdavidsfoundation.org");


                reader.Close();
                SendEmail(htRecipients, "Your 2019 Toast of the Town Party Selections have been <b>confirmed</b>.</br></br>Please visit https://toastconcierge.azurewebsites.net to view your party details. </br></br> In order to provide party hosts with a complete guest list, we will need you to complete your <font color=\"red\">party attendee information</font> as soon as possible.</br></br>If you have any questions please contact Ashley Gomez at agomez@stdavidsfoundation.org or 512-879-6218.</br></br>2019 Toast of the Town Team", "Toast of the Town 2019", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debug");
            }
            finally
            {
                conn.Close();
            }
        }

        static void ProcessRequestReminderTextQueue()
        {
            string selectSQL = string.Empty;

            selectSQL = "Select distinct Telephone from vwPartySlots";
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                string MobilePhone = "";
                Hashtable htRecipients = new Hashtable();
                while (reader.Read())
                {
                    MobilePhone = reader["Telephone"].ToString().Trim();
                    htRecipients.Add(MobilePhone, MobilePhone);
                }
                //htRecipients = new Hashtable();


                //htRecipients.Add("5125603310", "5125603310");
                //htRecipients.Add("5123286212", "5123286212");
                //htRecipients.Add("5125172340", "5125172340");
                //htRecipients.Add("5123587290", "5123587290");
                //htRecipients.Add("5126997211", "5126997211");
                //htRecipients.Add("5125274069", "5125274069");
                //htRecipients.Add("5127507445", "5127507445");
                //htRecipients.Add("5127893494", "5127893494");
                //htRecipients.Add("5127626708", "5127626708");
                //htRecipients.Add("5127502451", "5127502451");
                //htRecipients.Add("5124232423", "5124232423");
                //htRecipients.Add("5129636367", "5129636367");
                //htRecipients.Add("5126361164", "5126361164");
                //htRecipients.Add("5129175883", "5129175883");
                //htRecipients.Add("5124315063", "5124315063");


                //htRecipients.Add("4052503881", "4052503881");
                htRecipients.Add("9567398099", "9567398099");
                htRecipients.Add("5122305044", "5122305044");
                htRecipients.Add("5129229868", "5129229868");

                

                reader.Close();

                SendText("Your Toast of the Town 2019 party confirmations are available.", htRecipients);
                SendText("Please visit https://toastconcierge.azurewebsites.net to see your party details and to fill out your party attendee information.", htRecipients);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debug");
            }
            finally
            {
                conn.Close();
            }
        }

        static void ProcessMessageQueue()
        {
            

            string selectSQL = string.Empty;

            selectSQL = "Select * from vwCCMessageQueue where Status = 'Pending' or Status is null";
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                string Description = "";
                string MessageName = "";
                string SendType = "";
                string PartyName = "";
                string RecipientType = "";
                string Days = "";
                string CompletedDate = "";

                while (reader.Read())
                {
                    Description = reader["Description"].ToString().Trim();
                    MessageName = reader["MessageName"].ToString().Trim();
                    SendType = reader["SendType"].ToString().Trim();
                    PartyName = reader["PartyDescription"].ToString().Trim();
                    RecipientType = reader["RecipientType"].ToString().Trim();
                    Days = reader["Days"].ToString().Trim();
                    CompletedDate = reader["CompletedDate"].ToString().Trim();

                    DateTime dtNow = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    if (string.IsNullOrEmpty(CompletedDate))
                        CompletedDate = "1/1/2000";
                    DateTime dtPartyDate = Convert.ToDateTime(CompletedDate);
                    dtPartyDate = Convert.ToDateTime(dtPartyDate.ToShortDateString());
                    int nNumDays = (dtNow - dtPartyDate).Days;
                    if (nNumDays > 0)
                    {
                        ProcessPartyReminder(MessageName, SendType, PartyName, RecipientType, Days);
                        UpdateLastCompletedTime(Description);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debug");
            }
            finally
            {
                conn.Close();
            }
        }

        static void ProcessPartyReminder(string MessageName, string SendType, string PartyName, string RecipientType, string Days)
        {
            Hashtable htParties = new Hashtable();

            htParties = GetAllParties(PartyName);

            if (htParties.Count > 0)
            {
                DateTime dtNow = DateTime.Now;
                foreach (DictionaryEntry itm in htParties)
                {
                    DateTime dtPartyDate = Convert.ToDateTime(itm.Value);
                    int nNumDays = (dtPartyDate - dtNow).Days;
                    if (nNumDays == Convert.ToInt32(Days) || Convert.ToInt32(Days) == -1)
                    {
                        SendMessage(MessageName, RecipientType, itm.Key.ToString());
                    }

                }
                
            }
            Console.WriteLine("Debug");
        }

        static Hashtable GetAllParties(string PartyName)
        {
            Hashtable htParties = new Hashtable();

            if (PartyName.ToUpper() == "ALL")
            {
                string selectSQL = string.Empty;

                selectSQL = "Select Description, EventDate from vwParties";
                string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand(selectSQL, conn);
                try
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    string Description = "";
                    string EventDate = "";


                    while (reader.Read())
                    {
                        Description = reader["Description"].ToString().Trim();
                        EventDate = reader["EventDate"].ToString().Trim();
                        htParties.Add(Description, EventDate);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Debug");
                }
                finally
                {
                    conn.Close();
                }
            }
            else if (PartyName.ToUpper() == "PRELUDES")
            {
                htParties.Add("Preludes", "2/14/2019");
            }
            else
            {
            }
            return htParties;
        }


        static Hashtable GetPartyRecipients(string PartyName, string SendType, string RecipientType)
        {
            string selectSQL = string.Empty;

            if (SendType.ToUpper() == "TEXT")
            {
                if (PartyName == "Preludes")
                    selectSQL = "Select Distinct  Name, Telephone as ContactMethod from [dbo].[vwAttendeePreload]  where Telephone is not null and preludes = 1";
                else
                {
                    if (RecipientType == "All Attendees")
                        selectSQL = "Select Distinct  AttendeeName as name, TRIM(Replace(Replace(Replace(Replace(Replace(AttendeeMobile,'-',''),'.',''),' ',''),'(',''),')','')) as ContactMethod from [dbo].[vwPartyAttendees]  where  Description = '" + PartyName + "'";
                    else if (RecipientType == "Main Party Contact")
                        selectSQL = "Select Distinct  Name, Telephone as ContactMethod from [dbo].[vwPartyAttendees]  where Telephone is not null and Description = '" + PartyName + "'  and (attendeename is null or attendeename = '' or attendeemobile is null or attendeemobile = '' or attendeeemail is null or attendeeemail = '')";
                    else
                        selectSQL = "Select Distinct  Name, Telephone as ContactMethod from [dbo].[vwPartyAttendees]  where Telephone is not null and Description = '" + PartyName + "'  and (attendeename is null or attendeename = '' or attendeemobile is null or attendeemobile = '' or attendeeemail is null or attendeeemail = '')";
                }
            }
            else
            {
                if (PartyName == "Preludes")
                    selectSQL = "Select Distinct  Name, Email as ContactMethod from [dbo].[vwAttendeePreload]  where email is not null and preludes = 1";
                else
                {
                    if (RecipientType == "All Attendees")
                        selectSQL = "Select Distinct AttendeeName as name, AttendeeEmail as ContactMethod from [dbo].[vwPartyAttendees]  where Description = '" + PartyName + "'";
                    else if (RecipientType == "Main Party Contact")
                        selectSQL = "Select Distinct name, email as ContactMethod from [dbo].[vwPartyAttendees]  where Email is not null and  Description = '" + PartyName + "'";
                    else
                        selectSQL = "Select Distinct Name, Email as ContactMethod from [dbo].[vwPartyAttendees]  where Email is not null and Description = '" + PartyName + "' and (attendeename is null or attendeename = '' or attendeemobile is null or attendeemobile = '' or attendeeemail is null or attendeeemail = '')";
                }
                
            }


            Hashtable htReceipients = new Hashtable();
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                string Name = "";
                string ContactMethod = "";

                while (reader.Read())
                {
                    Name = reader["Name"].ToString().Trim();
                    ContactMethod = reader["ContactMethod"].ToString().Trim();
                    htReceipients.Add(Name, ContactMethod);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debug");
            }
            finally
            {
                conn.Close();
            }

            //htReceipients = new Hashtable(); //Debugging Purposes

            if (SendType.ToUpper() == "TEXT")
            {
                htReceipients.Add("Jimmy Voegele", "4052503881");
                htReceipients.Add("9567398099", "9567398099");
                htReceipients.Add("5122305044", "5122305044");
                htReceipients.Add("5129229868", "5129229868");
            }
            else
            {
                htReceipients.Add("Jimmy Voegele", "jimmy.voegele@gmail.com");
            }

            return htReceipients;
        }

        static void SendMessage(string MessageName, string RecipientType, string PartyName)
        {
            string selectSQL = string.Empty;

            selectSQL = "Select * from vwCCMessageTemplates where Description  = '" + MessageName + "'";
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                string MessageType = "";
                string MessageSubject = "";
                string MessageBody = "";


                while (reader.Read())
                {
                    MessageType = reader["Type"].ToString().Trim();
                    MessageSubject = reader["Subject"].ToString().Trim();
                    MessageSubject = ReplaceKeywords(MessageSubject, PartyName);
                    MessageBody = reader["Message"].ToString().Trim();
                    MessageBody = ReplaceKeywords(MessageBody, PartyName);
                    if (MessageType.ToUpper() == "TEXT")
                    {
                        Hashtable ht = GetPartyRecipients(PartyName, MessageType, RecipientType);
                        SendText(MessageBody, ht);
                    }
                    else
                    {
                        Hashtable ht = GetPartyRecipients(PartyName, MessageType, RecipientType);
                        SendEmail(ht, MessageBody, MessageSubject);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debug");
            }
            finally
            {
                conn.Close();
            }
        }

        static string ReplaceKeywords(string strMessage, string strPartyName)
        {
            string strNewMessage = strMessage.Replace("{PARTYNAME}", strPartyName);
    
            return strNewMessage;
        }


        static void SendText(string strMessage, Hashtable htRecipients)
        {
            DialMyCalls dmc = new DialMyCalls();

            //dmc.LoadAPIKey("a7662b8c133cc3b3f73593c61e5d97a2");
            dmc.LoadAPIKey("17bbc62cf862e37ec6d3b284ef26f25e");

            var Recordings = dmc.GetRecordings();

            //Guid g = new Guid("75c07876-02b7-11e8-aa3a-0cc47a8124e8");
            Guid g = new Guid("3353d448-d84b-11e6-ab39-52540062a4cf");


            List<ContactAttributesEX> lstContacts = new List<ContactAttributesEX>();
            foreach (DictionaryEntry dct in htRecipients)
            {
                string strPhoneNumber = dct.Value.ToString();
                ContactAttributesEX contact = new ContactAttributesEX(strPhoneNumber, dct.Key.ToString(), "", "", 1, false);

                lstContacts.Add(contact);
            }
            dmc.SendText("Toast of the Town", g, strMessage, lstContacts);
        }

        static void SendEmail(Hashtable htRecipients,  string strMessage, string strSubject, bool bHighPriority = false)
        { 

            String userName = System.Configuration.ConfigurationSettings.AppSettings["MailUsername"].ToString();
            String password = System.Configuration.ConfigurationSettings.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();

            string strFrom = "Toast Concierge <toastrequests@stdavidsfoundation.org>";
             MailAddress from = new MailAddress(strFrom);

            mail.From = from;
            mail.Subject = strSubject;



            mail.IsBodyHtml = false;
            mail.Body = strMessage;
            if (bHighPriority)
                mail.Priority = MailPriority.High;

            foreach (DictionaryEntry dct in htRecipients)
            {
                string strEmail = dct.Value.ToString();
                mail.Bcc.Add(strEmail);
            }

            //AddEmailAddresses(mail, "ErrorEmailAddresses");

           mail.To.Add("AGomez@stdavidsfoundation.org");

            string strEmail2 = "jimmy@veicorporation.com";
            mail.Bcc.Add(strEmail2);

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void AddEmailAddresses(MailMessage mail, string strSection)
        {
            String strSummaryEmailAddresses = System.Configuration.ConfigurationSettings.AppSettings[strSection].ToString();

            string[] strAddresses = strSummaryEmailAddresses.Split(';');
            foreach (string strTo in strAddresses)
            {
                MailAddress to = new MailAddress(strTo);
                mail.To.Add(to);
            }
        }

        static void UpdateLastCompletedTime(string strMessageDescription)
        {
            string selectSQL = "Update CCMessageQueue Set CompletedDate = '" + DateTime.Now.ToString() + "' where Description = '" + strMessageDescription + "'";
            string strConnectionString = System.Configuration.ConfigurationSettings.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }
    }


}
